<?php echo "Pattern 1:";?><br><br>
<?php

    for($i=1; $i<=5; $i++)
    {
    for($j=4; $j>=$i; $j--)  
    {
    echo '&nbsp;&nbsp;';
    }
    for($k=1; $k<=$i; $k++)  
    {
    echo '*';
    }
    echo '<br>';
    }

?>

<?php echo "Pattern 2:";?><br><br>
<?php 
    echo "<pre>";
    $n = 5;
    for ($i = 5; $i > 0; $i--)
    {
        for ($j = $n - $i; $j > 0; $j--)
            echo "&nbsp;&nbsp;";
        for ($j = 2 * $i - 1; $j > 0; $j--)
            echo ("&nbsp;*");
        echo "<br>";
    }
    echo "</pre>";
?>

<?php echo "Pattern 3:";?><br><br>
<?php 
    $n = 6; 
    ReverseCharBridge($n);
    function ReverseCharBridge($n) 
    { 
        
        for ($i = 1; $i < $n; $i++)  
        { 
            for ($j = 65; $j < 65 + 
                    (2 * $n) - 1; $j++)  
            { 
                if ($j >= (65 + $n - 1) + $i) 
                    echo chr((65 + $n - 1)-  
                        ($j % (65 + $n - 1)))."&nbsp;"; 
                          
                else if ($j <= (65 + $n - 1) - $i) 
                    echo chr($j)."&nbsp;"; 

                else
                    echo "&nbsp;&nbsp; "; 
            } 
            echo "\n"."<br><br><br>"."&nbsp;"; 
        } 
    } 
?>

<?php echo "Pattern 4:";?><br><br><br>
<?php
    $column = 1;
    $rows = 5;
     
    for($x = 0; $x < $rows; $x++)
    {
        for($space = 1; $space <= $rows - $x; $space++)
            echo ("&nbsp;&nbsp;&nbsp;");
     
        for($j=0; $j <= $x; $j++)
        {
            if ($j == 0 || $x == 0)
                $column = 1;
            else
                $column = $column*($x - $j+1)/$j;
     
            echo ($column."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
        }
        echo ("<br>"); 
    }
?>

<?php echo "Pattern 5:";?><br><br>
<?php
    for($i=0;$i<=4;$i++)
    {
    echo "&nbsp;&nbsp;";
    }
    for($i=0;$i<=4;$i++)
    {

      echo "* ";
    }
   for ($row = 1; $row <= 5; $row++)
    {
      echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp* </br>";
    }

   for($j=0;$j<=5;$j++)
    {
     echo "* ";
    }

    for($i=0;$i<=4;$i++)
    {
      echo "* ";
    }
    for ($row = 1; $row <= 5; $row++)
    {
      echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp* </br>";
    }
    for($i=0;$i<=4;$i++)
    {
        echo "* ";
    }
?>

